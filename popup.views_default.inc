<?php
  /*
   * $Id: popup.views_default.inc 10 2013-02-26 21:10:45Z Gerson.junior $
   */

function popup_views_default_views()
{
	$views = array();

	$view = new view();
	$view->name = 'popup_home';
	$view->description = '';
	$view->tag = 'default';
	$view->base_table = 'node';
	$view->human_name = 'Popup home';
	$view->core = 7;
	$view->api_version = '3.0';
	$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */	
	
/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['use_more_text'] = 'mais';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['submit_button'] = 'Aplicar';
$handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Reiniciar';
$handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Ordenar por';
$handler->display->display_options['exposed_form']['options']['sort_asc_label'] = 'Ascendente';
$handler->display->display_options['exposed_form']['options']['sort_desc_label'] = 'Decrescente';
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '1';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['row_plugin'] = 'fields';
/* Campo: Conteúdo: Título do popup */
$handler->display->display_options['fields']['field_popup_titulo']['id'] = 'field_popup_titulo';
$handler->display->display_options['fields']['field_popup_titulo']['table'] = 'field_data_field_popup_titulo';
$handler->display->display_options['fields']['field_popup_titulo']['field'] = 'field_popup_titulo';
$handler->display->display_options['fields']['field_popup_titulo']['label'] = '';
$handler->display->display_options['fields']['field_popup_titulo']['element_label_colon'] = FALSE;
/* Campo: Conteúdo: Texto do pop-up */
$handler->display->display_options['fields']['field_popup_texto']['id'] = 'field_popup_texto';
$handler->display->display_options['fields']['field_popup_texto']['table'] = 'field_data_field_popup_texto';
$handler->display->display_options['fields']['field_popup_texto']['field'] = 'field_popup_texto';
$handler->display->display_options['fields']['field_popup_texto']['label'] = '';
$handler->display->display_options['fields']['field_popup_texto']['element_label_colon'] = FALSE;
/* Campo: Conteúdo: Data de publicação */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'node';
$handler->display->display_options['fields']['created']['field'] = 'created';
/* Critério de ordenação: Conteúdo: Data de publicação */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Critério de filtragem: Conteúdo: Publicado */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Critério de filtragem: Conteúdo: Tipo */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'popup' => 'popup',
);

/* Display: Bloco */
$handler = $view->new_display('block', 'Bloco', 'block_1');
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Campo: Conteúdo: Exibir com o fundo preto? */
$handler->display->display_options['fields']['field_popup_fundo']['id'] = 'field_popup_fundo';
$handler->display->display_options['fields']['field_popup_fundo']['table'] = 'field_data_field_popup_fundo';
$handler->display->display_options['fields']['field_popup_fundo']['field'] = 'field_popup_fundo';
$handler->display->display_options['fields']['field_popup_fundo']['label'] = '';
$handler->display->display_options['fields']['field_popup_fundo']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_popup_fundo']['element_label_colon'] = FALSE;
/* Campo: Conteúdo: Duração */
$handler->display->display_options['fields']['field_popup_duracao']['id'] = 'field_popup_duracao';
$handler->display->display_options['fields']['field_popup_duracao']['table'] = 'field_data_field_popup_duracao';
$handler->display->display_options['fields']['field_popup_duracao']['field'] = 'field_popup_duracao';
$handler->display->display_options['fields']['field_popup_duracao']['label'] = '';
$handler->display->display_options['fields']['field_popup_duracao']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_popup_duracao']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_popup_duracao']['hide_empty'] = TRUE;
$handler->display->display_options['fields']['field_popup_duracao']['empty_zero'] = TRUE;
/* Campo: Conteúdo: Título do popup */
$handler->display->display_options['fields']['field_popup_titulo']['id'] = 'field_popup_titulo';
$handler->display->display_options['fields']['field_popup_titulo']['table'] = 'field_data_field_popup_titulo';
$handler->display->display_options['fields']['field_popup_titulo']['field'] = 'field_popup_titulo';
$handler->display->display_options['fields']['field_popup_titulo']['label'] = '';
$handler->display->display_options['fields']['field_popup_titulo']['element_label_colon'] = FALSE;
/* Campo: Conteúdo: Texto do pop-up */
$handler->display->display_options['fields']['field_popup_texto']['id'] = 'field_popup_texto';
$handler->display->display_options['fields']['field_popup_texto']['table'] = 'field_data_field_popup_texto';
$handler->display->display_options['fields']['field_popup_texto']['field'] = 'field_popup_texto';
$handler->display->display_options['fields']['field_popup_texto']['label'] = '';
$handler->display->display_options['fields']['field_popup_texto']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['field_popup_texto']['alter']['text'] = '<div class="fundo-preto-[field_popup_fundo]">
	<div id="popup-duracao">[field_popup_duracao]</div>
	[field_popup_texto]
	</div>';
$handler->display->display_options['fields']['field_popup_texto']['element_label_colon'] = FALSE;
/* Campo: Global: PHP */
$handler->display->display_options['fields']['php']['id'] = 'php';
$handler->display->display_options['fields']['php']['table'] = 'views';
$handler->display->display_options['fields']['php']['field'] = 'php';
$handler->display->display_options['fields']['php']['label'] = '';
$handler->display->display_options['fields']['php']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['php']['use_php_setup'] = 0;
$handler->display->display_options['fields']['php']['php_output'] = '<?php
print \'<form><input class="lembrar" type="checkbox" name="popup" value="\' . $data->nid . \'">  Não mostrar novamente</form>\';
?>';
$handler->display->display_options['fields']['php']['use_php_click_sortable'] = '0';
$handler->display->display_options['fields']['php']['php_click_sortable'] = '';
/* Campo: Conteúdo: Link "editar" */
$handler->display->display_options['fields']['edit_node']['id'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['table'] = 'views_entity_node';
$handler->display->display_options['fields']['edit_node']['field'] = 'edit_node';
$handler->display->display_options['fields']['edit_node']['label'] = '';
$handler->display->display_options['fields']['edit_node']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['edit_node']['text'] = 'Editar';
$handler->display->display_options['defaults']['arguments'] = FALSE;

/* Display: Página */
$handler = $view->new_display('page', 'Página', 'page_1');
$handler->display->display_options['defaults']['style_plugin'] = FALSE;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'field_popup_titulo' => 'field_popup_titulo',
  'field_popup_texto' => 'field_popup_texto',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'field_popup_titulo' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'field_popup_texto' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['defaults']['style_options'] = FALSE;
$handler->display->display_options['defaults']['row_plugin'] = FALSE;
$handler->display->display_options['defaults']['row_options'] = FALSE;
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Campo: Operações em massa: Conteúdo */
$handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
$handler->display->display_options['fields']['views_bulk_operations']['table'] = 'node';
$handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
$handler->display->display_options['fields']['views_bulk_operations']['label'] = '';
$handler->display->display_options['fields']['views_bulk_operations']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '0';
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
$handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
$handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
  'action::node_assign_owner_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::views_bulk_operations_delete_item' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::pathauto_node_update_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::node_unpublish_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::node_unpublish_by_keyword_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::node_make_sticky_action' => array(
    'selected' => 1,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::system_send_email_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::views_bulk_operations_script_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::views_bulk_operations_modify_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
    'settings' => array(
      'show_all_tokens' => 1,
      'display_values' => array(
        '_all_' => '_all_',
      ),
    ),
  ),
  'action::views_bulk_operations_argument_selector_action' => array(
    'selected' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
    'settings' => array(
      'url' => '',
    ),
  ),
  'action::node_promote_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::node_publish_action' => array(
    'selected' => 1,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::node_unpromote_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::node_make_unsticky_action' => array(
    'selected' => 0,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
  'action::node_save_action' => array(
    'selected' => 1,
    'postpone_processing' => 0,
    'skip_confirmation' => 0,
    'override_label' => 0,
    'label' => '',
  ),
);
/* Campo: Conteúdo: Título */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
/* Campo: Conteúdo: Publicado */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'node';
$handler->display->display_options['fields']['status']['field'] = 'status';
$handler->display->display_options['fields']['status']['not'] = 0;
/* Campo: Conteúdo: Data de publicação */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'node';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['created']['date_format'] = 'short';
$handler->display->display_options['fields']['created']['second_date_format'] = 'long';
$handler->display->display_options['path'] = 'admin/administrar-popups';

  $views[$view->name] = $view;
  return $views;
}