(function ($) {

  Drupal.behaviors.popup = {
    attach: function(context, settings) {
		
    var banner = $("#block-popup-popup");

    //Checa se existe cookie do banner
    if(!$.cookie(banner.find('.lembrar').val())) {
        window.setTimeout(showBanner, 1500); /*faz o banner aparecer ap�s 1,5 seg*/
    }
	function showBanner(){
        banner.fadeIn();
        if(banner.find(".fundo-preto-Sim")[0]){
            $("body").append("<div id='fundo-escuro'>").fadeIn();
        }
        
    }

    /*faz o banner sumir após um valor cadastrado pelo usuário*/
    if($("#popup-duracao").html() && $("#popup-duracao").html() > 0){
        $timer = $("#popup-duracao").html() * 1000;
        window.setTimeout(hideBanner, $timer);
    }else{
        window.setTimeout(hideBanner, 600000);
    }
    
    function hideBanner(){
        banner.fadeOut();
        $("#fundo-escuro").fadeOut();
    }
    
    banner.children().append("<a id='bt-fechar' href='#'>X</a>"); /*Acrescenta o botão fechar*/

    $("#bt-fechar").click(function(){ /*Faz Funcionar o botão fechar*/
         hideBanner();
    });

    banner.find('.lembrar').change(function() {
        if ($(this).is(':checked')) {
            $.cookie($(this).val(), 'true', { expires: 365 });
        }
        else {
            $.removeCookie($(this).val()); 
        }
    });
    }
  };

})(jQuery);