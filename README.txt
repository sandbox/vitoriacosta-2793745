﻿INSTALLATION
----------

1. Unzip the .tar.gz module in the directory and Drupal modules.

2. Enable the module in " Administer > Site building > Modules " .

3. After installation is necessary to access the page blocks and insert the " Popup " block in any region ( Region recommended : " HIGHLIGHTED / HIGHLIGHT " ) .

USE
----------

- When the module is installed you will see a new content type called " Popup " .

- Add new content type " Popup " and choose which pages should appear .

- After creating a new content, the popup will be ready for use on selected pages .

PROBLEMS
---------

* The " POPUP " block does not appear ?

  - Make sure the block is enabled in an active site region.

  - Check for module viewing restrictions .


